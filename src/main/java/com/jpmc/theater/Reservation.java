package com.jpmc.theater;

public record Reservation(Customer customer, Showing showing, int audienceCount) {

    public double totalCost() {
        return showing.getNetPrice() * audienceCount;
    }

}