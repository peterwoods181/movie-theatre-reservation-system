package com.jpmc.theater;

public record Customer(String name, String id) {
}