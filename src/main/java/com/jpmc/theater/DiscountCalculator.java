package com.jpmc.theater;

import java.util.*;

/**
 * This class is used to calculate the discount for a given showing
 */
public class DiscountCalculator {

    /**
     * Each element in the discountsDefinitions list represents a declarative definition of a discount that can be
     * iterated through to calculate the largest discount for a given showing
     */
    private List<Discount> discountsDefinitions = Arrays.asList(Discount.values());

    /**
     * This method uses the steps below to calculate the largest discount amount
     *
     * 1) create a stream of all discounts
     * 2) filter for only discounts that are applicable to the showing
     * 3) calculate the total discount amount as flat amount + percent discount amount
     * 4) sort the discounts from highest to lowest
     * 5) return the highest discount, if there is no discount applicable default to 0
     * @param showing
     * @return
     */
    public double getLargestDiscountAmount(Showing showing) {
        return discountsDefinitions.stream()
                .filter(d -> d.isApplicable(showing))
                .map(d -> d.getFlatDiscountAmount() + showing.getMovie().ticketPrice() * d.getDiscountPercent() / 100)
                .sorted((a, b) -> Double.compare(b, a))
                .findFirst()
                .orElse(Double.valueOf(0));
    }

}
