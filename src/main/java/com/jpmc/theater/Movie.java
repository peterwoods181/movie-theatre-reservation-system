package com.jpmc.theater;

import java.time.Duration;

public record Movie(String title, Duration runningTime, double ticketPrice, int specialCode) {
}