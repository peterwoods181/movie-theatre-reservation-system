package com.jpmc.theater;

import java.time.LocalDateTime;

public class Showing {
    private Movie movie;
    private int sequenceOfTheDay;
    private LocalDateTime showStartTime;
    private DiscountCalculator discountCalculator = new DiscountCalculator();

    public Showing(Movie movie, int sequenceOfTheDay, LocalDateTime showStartTime) {
        this.movie = movie;
        this.sequenceOfTheDay = sequenceOfTheDay;
        this.showStartTime = showStartTime;
    }

    public Movie getMovie() {
        return movie;
    }

    public LocalDateTime getStartTime() {
        return showStartTime;
    }

    public double getDiscount() {
        return discountCalculator.getLargestDiscountAmount(this);
    }

    public int getSequenceOfTheDay() {
        return sequenceOfTheDay;
    }

    public double getNetPrice() {
        return movie.ticketPrice() - this.getDiscount();
    }

}
