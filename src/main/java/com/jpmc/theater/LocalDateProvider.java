package com.jpmc.theater;

import java.time.LocalDate;

/**
 * An enum is used to ensure that there is only ever one instance of a "LocalDateProvider". This approach follows
 * the recommendation laid out in item #3 in "Effective Java" by Joshua Bloch
 */
public enum LocalDateProvider {
    INSTANCE;

    public LocalDate currentDate() {
        return LocalDate.now();
    }
}
