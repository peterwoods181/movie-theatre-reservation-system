package com.jpmc.theater;

public interface Applicable {
    boolean isApplicable(Showing showing);
}
