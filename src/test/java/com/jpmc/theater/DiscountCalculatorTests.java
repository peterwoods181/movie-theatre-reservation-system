package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DiscountCalculatorTests {

    /**
     * First showing and special movie are applicable in this scenario. The higher of the two is 20.0
     */
    @Test
    public void givenSeveralDiscountsApplicable_whenDiscountsCalculated_thenReturnLargestDiscount() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
        Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));
        var discountCalculator = new DiscountCalculator();

        var actualDiscountAmount = discountCalculator.getLargestDiscountAmount(showing);
        var expectedDiscountAmount = 20.0;

        assert (Double.compare(actualDiscountAmount, expectedDiscountAmount) == 0);
    }

    /**
     * Only the first showing discount is applicable
     */
    @Test
    public void givenSingleDiscountApplicable_whenDiscountsCalculated_ThenReturnSingleDiscount() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));
        var discountCalculator = new DiscountCalculator();

        var actualDiscountAmount = discountCalculator.getLargestDiscountAmount(showing);
        var expectedDiscountAmount = 3.0;

        assert (Double.compare(actualDiscountAmount, expectedDiscountAmount) == 0);
    }


    @Test
    public void givenNoDiscountsApplicable_whenDiscountsCalculated_ThenReturnZeroAsDiscount() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 3, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));
        var discountCalculator = new DiscountCalculator();

        var actualDiscountAmount = discountCalculator.getLargestDiscountAmount(showing);
        var expectedDiscountAmount = 0.0;

        assert (Double.compare(actualDiscountAmount, expectedDiscountAmount) == 0);

    }


}
