package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShowingTests {

    @Test
    void givenDiscountIsCalculated_whenGetDiscountIsCalled_thenReturnCorrectDiscount() {
        var showing = new Showing(
                new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1),
                1,
                LocalDateTime.now()
        );

        showing.getDiscount();
        var expectedResult = 3.125;
        var acutalRestult = showing.getDiscount();
        assertTrue(Double.compare(expectedResult, acutalRestult) == 0);
    }

}
