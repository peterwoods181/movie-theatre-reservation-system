package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReservationTests {

    @Test
    void givenTotalCostIsCalculated_whenTotalCostIsCalled_thenReturnCorrectTotalCost() {
        var customer = new Customer("John Doe", "unused-id");
        var showing = new Showing(
                new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1),
                1,
                LocalDateTime.now()
        );
        var reservation = new Reservation(customer, showing, 3);

        var expectedResult = 28.125;
        var acutalRestult = reservation.totalCost();
        assertTrue(Double.compare(expectedResult, acutalRestult) == 0);
    }
}
