package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TheaterTests {

    @Test
    void givenReservationIsvalid_whenReserveIsCalled_thenReturnReservation() {
        Theater theater = new Theater(LocalDateProvider.INSTANCE);
        Customer john = new Customer("John Doe", "id-12345");
        assertDoesNotThrow(
                ()->{
                   Reservation reservation = theater.reserve(john, 5, 4);
                });
    }

    @Test
    void givenReservationIsNOTvalid_whenReserveIsCalled_thenReturnIllegalStateException() {
        Theater theater = new Theater(LocalDateProvider.INSTANCE);
        Customer john = new Customer("John Doe", "id-12345");
        assertThrows(IllegalStateException.class,
                ()->{
                    theater.reserve(john, -5, 4);
                });
    }

}
