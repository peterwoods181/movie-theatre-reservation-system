package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class LocalDateProviderTests {

    @Test
    void givenLocalDateProviderReturnsCurrentLocalDate_whenCurrentDateIsCalled_thenReturnCorrectLocalDate() {
        var expectedResult = LocalDate.now();
        var actualResult = LocalDateProvider.INSTANCE.currentDate();

        assert(expectedResult.compareTo(actualResult) == 0);
    }
}
