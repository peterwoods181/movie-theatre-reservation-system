package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DiscountTests {

    @Test
    public void givenMovieIsSpecial_whenDiscountApplicable_thenReturnSpecialMovieDiscountApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
        Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SPECIAL_MOVIE.isApplicable(showing);
        var expectedDiscountApplicable = true;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsNOTSpecial_whenDiscountApplicable_thenReturnSpecialMovieDiscountNOTApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SPECIAL_MOVIE.isApplicable(showing);
        var expectedDiscountApplicable = false;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsFirstShowing_whenDiscountApplicable_thenReturnFirstShowingDiscountApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
        Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.FIRST_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = true;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsNOTFirstShowing_whenDiscountApplicable_thenReturnFirstShowingDiscountNOTApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.FIRST_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = false;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsSecondShowing_whenDiscountApplicable_thenReturnSecondShowingDiscountApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
        Showing showing = new Showing(spiderMan, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SECOND_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = true;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsNOTSecondShowing_whenDiscountApplicable_thenReturnSecondShowingDiscountNOTApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 3, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SECOND_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = false;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsSeventhShowing_whenDiscountApplicable_thenReturnSeventhShowingDiscountApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 7, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SEVENTH_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = true;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsNOTSeventhShowing_whenDiscountApplicable_thenReturnSeventhShowingDiscountNOTApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 6, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

        var actualDiscountApplicable = Discount.SEVENTH_SHOWING.isApplicable(showing);
        var expectedDiscountApplicable = false;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsMatinee_whenDiscountApplicable_thenReturnMatineeDiscountApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 7, LocalDateTime.of(LocalDate.now(), LocalTime.of(12, 0)));

        var actualDiscountApplicable = Discount.MATINEE.isApplicable(showing);
        var expectedDiscountApplicable = true;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

    @Test
    public void givenMovieIsNOTMatinee_whenDiscountApplicable_thenReturnMatineeDiscountNOTApplicable() {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
        Showing showing = new Showing(spiderMan, 6, LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 0)));
        System.out.println(showing.getStartTime().getHour());

        var actualDiscountApplicable = Discount.MATINEE.isApplicable(showing);
        var expectedDiscountApplicable = false;

        assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
    }

}
